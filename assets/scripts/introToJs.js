/**
 * Give at least three reasons why you should learn javascript.
 */ 

//1. Better user experience, faster and more interactive responsive web pages.
//2. Easy to learn, Easy to implement
//3. Javascript is everywhere

/**
 * In two sentences, state in your own words, what is your understanding about Javascript?
 */

//1. Mind 
//2. Processor of the website

/**
 * What is a variable? Give at least three examples.
 */

// 1.
// Example #1:
// 	var x = 5;
//  var y = 6;
//  var z = x + y;
// 	
// 	Example #2:
//  let student = "Brandon";
//  console.log(student);
//
//  Example #3:
//  const day = "Monday";
//  console.log(day);

/**
 * What are the data types we talked about today? Give at least two samples each.
 */

// ===> Here String - denoted by qoutation marks:
// - Series of characters. 

// 2. Number - Can be used in mathematical operations
// Ex: 2, 3.15, 0.25, 450



/**
 * Write the correct data type of the following:
 */

// 1. "Number" - String
// 2. 158 - Number
// 3. pi - Undefined
// 4. false - Boolean
// 5. 'true' - value
// 6. 'Not a String' - value

/**
 * What are the arithmetic operators?
 */  //+, -, *, /

/**
 * What is a modulo? 
 */ // % - modulo = remainder

/**
 * Interpret the following code
 */

// let number = 4;
// number += 4;
// number = 4
// number (4) += 4
// number = 8

// Q: What is the value of number?
//ANS: = 8
// ----

// let number = 4;
// number -= 4;
// number (4) -= 4
// number = -4

// Q: What is the value of number?
// ANS: -4
// ----

// let number = 4;
// number *= 4;
// number (4) *= 4 
// number = 8
// 
// Q: What is the value of number?
// ANS: 8
// ----

// let number = 4;
// number /= 4;
// number (4) /= 4
// number = 1

// Q: What is the value of number?
// ANS: 1