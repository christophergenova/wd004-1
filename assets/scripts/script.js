console.log("hello");


// To declare a variable, we'll use the keywords let, or const:
let student = "Brandon";
// console.log(student);
// let is re-assinging us a value

const day = "Monday";
console.log(day);
// const is short for constant.
// We cannot reassign a value.

student ="Brenda";
// console.log(student);

student = 3.1416;
// console.log(student);

// Data Types:
// 1. String - denoted by qoutation marks:
// - Series of characters. 
// Ex: "brandon", "1234", "String"
// every quotation mark is a STRING

// 2. Number - Can be used in mathematical operations
// Ex: 2, 3.15, 0.25, 450

// 3. Boolean - True or false.

// To check data type
console.log(typeof "Am I a String?");
console.log(typeof "1234"); // string
console.log(typeof 1234); // number
console.log(typeof "true"); //string
console.log(typeof false); // boolean
// console.log(typeof whatami); // undefined
//
//
// Javascript Operators
//
// Arithmetic Operators
// +, -, *, /
//
const num1 = 10;
const num2 = 2;
const num3 = 6;

console.log(num1 + num2);

// % - modulo = remainder
//
console.log(num1 % num2);

// Assignment Operator:
// = --> we are assigning a value to a variable;
//
// Assignment-Arithmetic;
// +=. -=, *=, /=, %=, 
//
let output = 0;
const num4 = 5;
const num5 = 8;

output = output + num4; // 5 = 0 + 5
output += num4;  // add num4 to the previous value of output;
// -= substract num4 from the previous value;




